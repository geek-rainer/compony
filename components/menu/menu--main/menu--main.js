Drupal.behaviors.menuMain = {
  attach: function (context, settings) {
    jQuery(function ($) {
        $("#js-burger-toggle").on("click", function() {
            if ($(".menu-item").hasClass("active")) {
                $(".menu-item").removeClass("active");
                $(this).find("a").html("<i class='fas fa-bars'></i>");
            } else {
                $(".menu-item").addClass("active");
                $(this).find("a").html("<i class='fas fa-times'></i>");
            }
        });
    });
  }
};
