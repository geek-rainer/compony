(($) => {
  Drupal.behaviors.enhanced_select = {
    attach: function attach(context, settings) {
      $('select').once('enhanced_select').select2({
        minimumResultsForSearch: 20
      });
    }
  };
})(jQuery, Drupal);
